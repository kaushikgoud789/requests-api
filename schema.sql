CREATE TABLE status(
    id serial PRIMARY KEY,
    name VARCHAR NOT NULL UNIQUE,
    description VARCHAR,
    dateupdated TIMESTAMPTZ);

CREATE TABLE approver(
    id serial PRIMARY KEY,
    name VARCHAR NOT NULL,
	orgname varchar not null,
	orgcode varchar not null,
	email varchar not null,
	phone varchar not null,
    isactive boolean not null default true,
    dateupdated TIMESTAMPTZ);

CREATE TABLE request(
    id serial PRIMARY KEY,
    requestedBy VARCHAR NOT NULL,
	orgname varchar not null,
	orgcode varchar not null,
	flatData json not null,
	requestDate TIMESTAMPTZ,
	amount double precision not null,
	status integer not null,
	approvedBy integer,
    dateupdated TIMESTAMPTZ,
	FOREIGN KEY (status) REFERENCES status(id),
    FOREIGN KEY (approvedBy) REFERENCES approver(id)
);

