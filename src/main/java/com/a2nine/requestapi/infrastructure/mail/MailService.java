package com.a2nine.requestapi.infrastructure.mail;

import java.io.IOException;
import java.time.Year;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.a2nine.requestapi.model.domain.Request;
import com.a2nine.requestapi.model.domain.postgres.Approver;

import freemarker.template.Template;
import freemarker.template.TemplateException;

@Component
public class MailService {

	private @Autowired JavaMailSender emailSender;
	private @Autowired freemarker.template.Configuration mailTemplateConfig;
	private @Value("${template.request-received-email-html}") String requestReceivedEmailTemplatePath;
	private @Value("${template.request-approval-email-html}") String requestApprovalEmailTemplatePath;
	private @Value("${template.request-status-updated-email-html}") String requestStatusUpdatedEmailTemplatePath;
	private @Value("${template.request-approver-confirmation-email-html}") String requestApprovalConfirmationEmailTemplatePath;

	@Async
	public void sendRequestEmail(Request request, List<Approver> approvers) {
		try {
			prepareAndSendRequestReceivedToRequestorEmail(request);
			prepareAndSendApprovaWaitingToApproverEmail(approvers, request);
		} catch (MessagingException | IOException | TemplateException e) {
			e.printStackTrace();
		}
	}

	@Async
	public void sendStatusChangeEmail(Request request, List<Approver> approvers) {
		try {
			prepareAndSendRequestStatusToRequestorEmail(request);
			prepareAndSendStatusToApproverEmail(approvers, request);
		} catch (MessagingException | IOException | TemplateException e) {
			e.printStackTrace();
		}
	}

	private void prepareAndSendRequestReceivedToRequestorEmail(Request request)
			throws MessagingException, TemplateException, IOException {
		Map<String, String> map = prepareMap(request, request.requestedBy());
		MimeMessage message = prepareMessage(request.email(), map, requestReceivedEmailTemplatePath,
				"Request #" + request.id() + " is sent for Approval ");
		emailSender.send(message);
	}

	private void prepareAndSendRequestStatusToRequestorEmail(Request request)
			throws MessagingException, TemplateException, IOException {

		Map<String, String> map = prepareMap(request, request.requestedBy());
		map.put("status", request.status().name());
		map.put("approvedby", request.approver().name());
		MimeMessage message = prepareMessage(request.email(), map, requestStatusUpdatedEmailTemplatePath,
				"Request #" + request.id() + " status is Updated");
		emailSender.send(message);
	}

	private void prepareAndSendApprovaWaitingToApproverEmail(List<Approver> approvers, Request request)
			throws MessagingException, TemplateException, IOException {
		Map<String, String> map = prepareMap(request, "Approval Team");
		map.put("requestor", request.email());
		map.put("amount", String.valueOf(request.amount()));
		map.put("type", request.type());

		List<String> emails = approvers.stream().map(appr -> appr.getEmail())
				.collect(Collectors.toCollection(ArrayList::new));
		MimeMessage message = prepareMessage(emails.toArray(new String[emails.size()]), map,
				requestApprovalEmailTemplatePath,
				"Request #" + request.id() + " has been raised and is waiting for approval");
		emailSender.send(message);
	}

	private void prepareAndSendStatusToApproverEmail(List<Approver> approvers, Request request)
			throws MessagingException, TemplateException, IOException {
		Map<String, String> map = prepareMap(request, "Approval Team");
		map.put("requestor", request.requestedBy());
		map.put("amount", String.valueOf(request.amount()));
		map.put("type", request.type());

		List<String> emails = approvers.stream().map(appr -> appr.getEmail())
				.collect(Collectors.toCollection(ArrayList::new));
		MimeMessage message = prepareMessage(emails.toArray(new String[emails.size()]), map,
				requestApprovalConfirmationEmailTemplatePath, "Request #" + request.id() + " status is Updated ");
		emailSender.send(message);
	}

	private String buildTemplate(Map<String, String> model, String template) throws TemplateException, IOException {
		Template mailTemplate = mailTemplateConfig.getTemplate(template);
		return FreeMarkerTemplateUtils.processTemplateIntoString(mailTemplate, model);
	}

	private MimeMessage prepareMessage(String[] to, Map<String, String> map, String templatePath, String subject)
			throws MessagingException, TemplateException, IOException {
		MimeMessage message = emailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		helper.setTo(to);
		helper.setSubject(subject);
		helper.setText(buildTemplate(map, templatePath), true);
		return message;
	}

	private MimeMessage prepareMessage(String to, Map<String, String> map, String templatePath, String subject)
			throws MessagingException, TemplateException, IOException {
		MimeMessage message = emailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		helper.setTo(to);
		helper.setSubject(subject);
		helper.setText(buildTemplate(map, templatePath), true);
		return message;
	}

	private Map<String, String> prepareMap(Request request, String name) {
		Map<String, String> map = new HashMap<>();
		map.put("request_number", String.valueOf(request.id()));
		map.put("name", name);
		map.put("year", String.valueOf(Year.now().getValue()));
		return map;
	}

}
