package com.a2nine.requestapi.infrastructure.postgres;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.Repository;

import com.a2nine.requestapi.model.domain.postgres.Approver;

@org.springframework.stereotype.Repository
public interface PostgresApproverRepository extends Repository<Approver, Long> {

	@Transactional
	List<Approver> findByOrgcode(String orgcode);

}
