package com.a2nine.requestapi.infrastructure.postgres;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.a2nine.requestapi.model.domain.postgres.Request;

@org.springframework.stereotype.Repository
public interface PostgresRequestRepository extends Repository<Request, Long> {

	@Transactional
	List<Request> findByRequestedByAndOrgcode(String requestedBy, String orgcode);

	@Transactional
	Request findByIdAndRequestedByAndOrgcode(Long Id, String requestedBy, String orgcode);

	@Transactional
	Request save(Request request);

	@Transactional
	@Modifying
	@Query(value = "Update request set status=:status,approvedby=:approvedby where id=:id", nativeQuery = true)
	int updateRequest(@Param("status") Long status, @Param("approvedby") Long approvedby, @Param("id") Long id);
}
