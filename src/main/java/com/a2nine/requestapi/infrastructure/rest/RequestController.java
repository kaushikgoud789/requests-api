package com.a2nine.requestapi.infrastructure.rest;

import static com.a2nine.requestapi.util.ObjectSerializer.toJsonString;

import java.io.IOException;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import com.a2nine.requestapi.model.domain.Organisation;
import com.a2nine.requestapi.model.domain.Request;
import com.a2nine.requestapi.model.domain.Status;
import com.a2nine.requestapi.usecases.FindAndSaveRequest;

@Path("/")
public class RequestController {

	private final Logger log = LogManager.getLogger(RequestController.class);

	private @Autowired FindAndSaveRequest findAndSaveRequest;

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response createRequest(@RequestBody String jsonBody, @NotNull @QueryParam("requestedBy") String requestedBy,
			@NotNull @QueryParam("orgname") String orgname, @NotNull @QueryParam("orgcode") String orgcode,
			@NotNull @QueryParam("requestDate") String requestDate, @NotNull @QueryParam("amount") Double amount,
			@NotNull @QueryParam("statusId") Long statusId, @NotNull @QueryParam("type") String type,
			@NotNull @QueryParam("email") String email) {
		log.info("json body:" + jsonBody);
		try {
			return Response.status(Response.Status.OK.getStatusCode())
					.entity(toJsonString(findAndSaveRequest.saveRequest(prepareRequest(jsonBody, requestedBy, orgname,
							orgcode, requestDate, amount, statusId, type, email))))
					.build();
		} catch (Exception e) {
			log.error(e.getMessage());
			return Response.status(Response.Status.BAD_REQUEST.getStatusCode()).build();
		}
	}

	@POST
	@Path("/{requestnumber}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateRequest(@PathParam("requestnumber") Long requestnumber,
			@NotNull @QueryParam("requestedBy") String requestedBy, @NotNull @QueryParam("orgname") String orgname,
			@NotNull @QueryParam("orgcode") String orgcode, @NotNull @QueryParam("approvedby") Long approvedby,
			@NotNull @QueryParam("statusid") Long statusId) {
		try {
			return Response
					.status(Response.Status.OK.getStatusCode()).entity(toJsonString(findAndSaveRequest
							.updateRequest(requestnumber, requestedBy, orgname, orgcode, approvedby, statusId)))
					.build();
		} catch (Exception e) {
			log.error(e.getMessage());
			return Response.status(Response.Status.BAD_REQUEST.getStatusCode()).build();
		}
	}

	@GET
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response fetchRequestByRequestedByAndOrgcode(@NotNull @QueryParam("requestedBy") String requestedBy,
			@NotNull @QueryParam("orgCode") String orgCode) {
		try {
			return Response.status(Response.Status.OK.getStatusCode())
					.entity(toJsonString(findAndSaveRequest.fetchRequestsByRequestedByAndOrgCode(requestedBy, orgCode)))
					.build();
		} catch (IOException e) {
			return Response.status(Response.Status.FORBIDDEN.getStatusCode()).build();
		} catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()).build();
		}

	}

	@GET
	@Path("/{requestnumber}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response fetchRequestByRequestedId(@PathParam("requestnumber") Long requestNumber,
			@NotNull @QueryParam("requestedBy") String requestedBy, @NotNull @QueryParam("orgCode") String orgCode) {
		try {
			return Response.status(Response.Status.OK.getStatusCode())
					.entity(toJsonString(findAndSaveRequest.fetchByRequestNumber(requestNumber, requestedBy, orgCode)))
					.build();
		} catch (IOException e) {
			return Response.status(Response.Status.FORBIDDEN.getStatusCode()).build();
		} catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()).build();
		}

	}

	private Request prepareRequest(String jsonBody, String requestedBy, String orgname, String orgcode,
			String requestDate, Double amount, Long statusId, String type, String email) {
		return new Request(requestedBy, new Organisation(orgname, orgcode), jsonBody, null, amount,
				new Status(statusId), null, null, type, email);
	}

}
