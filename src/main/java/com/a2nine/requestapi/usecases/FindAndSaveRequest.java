package com.a2nine.requestapi.usecases;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.a2nine.requestapi.infrastructure.mail.MailService;
import com.a2nine.requestapi.infrastructure.postgres.PostgresApproverRepository;
import com.a2nine.requestapi.infrastructure.postgres.PostgresRequestRepository;
import com.a2nine.requestapi.model.domain.Request;
import com.a2nine.requestapi.model.domain.postgres.Status;

@Service
public class FindAndSaveRequest {

	private @Autowired PostgresRequestRepository requestRepository;
	private @Autowired PostgresApproverRepository approverRepository;
	private @Autowired MailService mailService;
	private @Autowired RestTemplate restTemplate;
	private @Value("${account-api-url}") String accounts_api_url;

	@Transactional
	public List<Request> fetchRequestsByRequestedByAndOrgCode(String requestedBy, String orgcode) {
		return requestRepository.findByRequestedByAndOrgcode(requestedBy, orgcode).stream()
				.map(req -> req.toDomainObject()).collect(Collectors.toCollection(ArrayList::new));
	}

	@Transactional
	public Request fetchByRequestNumber(Long requestNumber, String requestedBy, String orgcode) {
		return requestRepository.findByIdAndRequestedByAndOrgcode(requestNumber, requestedBy, orgcode).toDomainObject();
	}

	@Transactional
	public Request saveRequest(Request request) {
		Request savedRequest = requestRepository.save(requestMapper(request)).toDomainObject();
		mailService.sendRequestEmail(savedRequest, approverRepository.findByOrgcode(request.organisation().orgcode()));
		return savedRequest;
	}

	@Transactional
	public String updateRequest(Long requestnumber, String requestedBy, String orgname, String orgcode, Long approvedby,
			Long statusId) {
		String response = StringUtils.EMPTY;
		requestRepository.updateRequest(statusId, approvedby, requestnumber);

		Request request = requestRepository.findByIdAndRequestedByAndOrgcode(requestnumber, requestedBy, orgcode)
				.toDomainObject();
		// Convert the request to a transaction only for approved requests
		if (statusId == 3) {
			HttpEntity<String> requestUpdate = new HttpEntity<>(request.flatData(), prepareHeaders());
			response = restTemplate
					.exchange(accounts_api_url + "/transactions", HttpMethod.PUT, requestUpdate, String.class)
					.getBody();
		}
		mailService.sendStatusChangeEmail(request, approverRepository.findByOrgcode(request.organisation().orgcode()));
		return response;

	}

	private HttpHeaders prepareHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return headers;
	}

	private com.a2nine.requestapi.model.domain.postgres.Request requestMapper(Request request) {
		return new com.a2nine.requestapi.model.domain.postgres.Request(request.id(), request.requestedBy(),
				request.organisation().orgname(), request.organisation().orgcode(), request.flatData(),
				request.requestDate(), request.amount(), new Status(request.status().id(), request.status().name(),
						request.status().description(), request.status().dateupdated()),
				null, request.dateupdated(), request.type(), request.email());
	}
}
