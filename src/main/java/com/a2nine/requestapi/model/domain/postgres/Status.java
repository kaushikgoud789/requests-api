package com.a2nine.requestapi.model.domain.postgres;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Status implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3580323893377818618L;

	private long id;
	private String name;
	private String description;
	private String dateupdated;

	public Status() {

	}

	public Status(long id, String name, String description, String dateupdated) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.dateupdated = dateupdated;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "dateupdated")
	public String getDateupdated() {
		return dateupdated;
	}

	public void setDateupdated(String dateupdated) {
		this.dateupdated = dateupdated;
	}

	public com.a2nine.requestapi.model.domain.Status toDomainObject() {
		return new com.a2nine.requestapi.model.domain.Status(this.id, this.name, this.description, this.dateupdated);
	}

}
