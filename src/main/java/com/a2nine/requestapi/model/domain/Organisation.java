package com.a2nine.requestapi.model.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Organisation {

	private String orgname;
	private String orgcode;

	@JsonCreator
	public Organisation(@JsonProperty("orgname") String orgname, @JsonProperty("orgcode") String orgcode) {
		super();
		this.orgname = orgname;
		this.orgcode = orgcode;
	}

	public String orgname() {
		return orgname;
	}

	public String orgcode() {
		return orgcode;
	}
}
