package com.a2nine.requestapi.model.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Status {

	private Long id;
	private String name;
	private String description;
	private String dateupdated;

	@JsonCreator
	public Status(@JsonProperty("id") long id, @JsonProperty("name") String name,
			@JsonProperty("description") String description, @JsonProperty("dateupdated") String dateupdated) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.dateupdated = dateupdated;
	}

	public Status(long id) {
		this.id = id;
	}

	public Long id() {
		return id;
	}

	public String name() {
		return name;
	}

	public String description() {
		return description;
	}

	public String dateupdated() {
		return dateupdated;
	}

}
