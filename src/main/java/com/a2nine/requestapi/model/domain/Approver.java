package com.a2nine.requestapi.model.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Approver {

	private Long id;
	private String name;
	private Organisation organisation;
	private String email;
	private String phone;
	private Boolean isactive;
	private Date dateupdated;

	@JsonCreator
	public Approver(@JsonProperty("id") Long id, @JsonProperty("name") String name,
			@JsonProperty("organisation") Organisation organisation, @JsonProperty("email") String email,
			@JsonProperty("phone") String phone, @JsonProperty("isactive") Boolean isactive,
			@JsonProperty("dateupdated") Date dateupdated) {
		super();
		this.id = id;
		this.name = name;
		this.organisation = organisation;
		this.email = email;
		this.phone = phone;
		this.isactive = isactive;
		this.dateupdated = dateupdated;
	}
	
	public Approver(Long id) {
		this.id = id;
	}

	public Long id() {
		return id;
	}

	public String name() {
		return name;
	}

	public Organisation organisation() {
		return organisation;
	}

	public String email() {
		return email;
	}

	public String phone() {
		return phone;
	}

	public Boolean isactive() {
		return isactive;
	}

	public Date dateupdated() {
		return dateupdated;
	}

}
