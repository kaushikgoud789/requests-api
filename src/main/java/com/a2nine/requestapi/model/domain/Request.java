package com.a2nine.requestapi.model.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Request {

	private long id;
	private String requestedBy;
	private Organisation organisation;
	private String flatData;
	private Date requestDate;
	private Double amount;
	private Status status;
	private Approver approver;
	private Date dateupdated;
	private String email;
	private String type;

	@JsonCreator
	public Request(@JsonProperty("id") long id, @JsonProperty("requestedBy") String requestedBy,
			@JsonProperty("organisation") Organisation organisation, @JsonProperty("flatData") String flatData,
			@JsonProperty("requestDate") Date requestDate, @JsonProperty("amount") Double amount,
			@JsonProperty("status") Status status, @JsonProperty("approver") Approver approver,
			@JsonProperty("dateupdated") Date dateupdated, @JsonProperty("email") String email,
			@JsonProperty("type") String type) {
		super();
		this.id = id;
		this.requestedBy = requestedBy;
		this.organisation = organisation;
		this.flatData = flatData;
		this.requestDate = requestDate;
		this.amount = amount;
		this.status = status;
		this.approver = approver;
		this.dateupdated = dateupdated;
		this.email = email;
		this.type = type;
	}

	public Request(String requestedBy, Organisation organisation, String flatData, Date requestDate, Double amount,
			Status status, Approver approver, Date dateupdated, String email, String type) {
		super();
		this.requestedBy = requestedBy;
		this.organisation = organisation;
		this.flatData = flatData;
		this.requestDate = requestDate;
		this.amount = amount;
		this.status = status;
		this.approver = approver;
		this.dateupdated = dateupdated;
		this.email = email;
		this.type = type;
	}

	public Long id() {
		return id;
	}

	public String requestedBy() {
		return requestedBy;
	}

	public Organisation organisation() {
		return organisation;
	}

	public String flatData() {
		return flatData;
	}

	public Date requestDate() {
		return requestDate;
	}

	public Double amount() {
		return amount;
	}

	public Status status() {
		return status;
	}

	public Approver approver() {
		return approver;
	}

	public Date dateupdated() {
		return dateupdated;
	}

	public String email() {
		return email;
	}

	public String type() {
		return type;
	}
}
