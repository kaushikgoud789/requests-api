package com.a2nine.requestapi.model.domain.postgres;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.a2nine.requestapi.model.domain.Organisation;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;

@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Request implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6621737250699972667L;

	private long id;
	private String requestedBy;
	private String orgname;
	private String orgcode;
	private String flatData;
	private Date requestDate;
	private Double amount;
	private Status status;
	private Approver approver;
	private Date dateupdated;
	private String type;
	private String email;
	public Request() {

	}

	public Request(long id, String requestedBy, String orgname, String orgcode, String flatData, Date requestDate,
			Double amount, Status status, Approver approver, Date dateupdated,String type,String email) {
		super();
		this.id = id;
		this.requestedBy = requestedBy;
		this.orgname = orgname;
		this.orgcode = orgcode;
		this.flatData = flatData;
		this.requestDate = requestDate;
		this.amount = amount;
		this.status = status;
		this.approver = approver;
		this.dateupdated = dateupdated;
		this.type = type;
		this.email = email;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "requestedby")
	public String getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	@Column(name = "orgname")
	public String getOrgname() {
		return orgname;
	}

	public void setOrgname(String orgname) {
		this.orgname = orgname;
	}

	@Column(name = "orgcode")
	public String getOrgcode() {
		return orgcode;
	}

	public void setOrgcode(String orgcode) {
		this.orgcode = orgcode;
	}

	@Type(type = "jsonb")
	@Column(name = "flatdata", columnDefinition = "jsonb")
	public String getFlatData() {
		return flatData;
	}

	public void setFlatData(String flatData) {
		this.flatData = flatData;
	}

	@Column(name = "requestdate")
	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	@Column(name = "amount")
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "status")
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "approvedby")
	public Approver getApprover() {
		return approver;
	}

	public void setApprover(Approver approver) {
		this.approver = approver;
	}

	@Column(name = "dateupdated")
	public Date getDateupdated() {
		return dateupdated;
	}

	public void setDateupdated(Date dateupdated) {
		this.dateupdated = dateupdated;
	}

	@Column(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public com.a2nine.requestapi.model.domain.Request toDomainObject() {
		return new com.a2nine.requestapi.model.domain.Request(this.id, this.requestedBy,
				new Organisation(this.orgname, this.orgcode), this.flatData, this.requestDate, this.amount,
				this.status.toDomainObject(), null != this.approver ? this.approver.toDomainObject() : null,
				this.dateupdated,this.type,this.email);
	}
}
