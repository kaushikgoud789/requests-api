package com.a2nine.requestapi.model.domain.postgres;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.a2nine.requestapi.model.domain.Organisation;

@Entity
public class Approver implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4369026925317442161L;

	private Long id;
	private String name;
	private String orgname;
	private String orgcode;
	private String email;
	private String phone;
	private Boolean isactive;
	private Date dateupdated;

	public Approver() {

	}

	public Approver(long id) {
		this.id = id;
	}

	public Approver(Long id, String name, String orgname, String orgcode, String email, String phone, Boolean isactive,
			Date dateupdated) {
		super();
		this.id = id;
		this.name = name;
		this.orgname = orgname;
		this.orgcode = orgcode;
		this.email = email;
		this.phone = phone;
		this.isactive = isactive;
		this.dateupdated = dateupdated;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "orgname")
	public String getOrgname() {
		return orgname;
	}

	public void setOrgname(String orgname) {
		this.orgname = orgname;
	}

	@Column(name = "orgcode")
	public String getOrgcode() {
		return orgcode;
	}

	public void setOrgcode(String orgcode) {
		this.orgcode = orgcode;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "phone")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "isactive")
	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	@Column(name = "dateupdated")
	public Date getDateupdated() {
		return dateupdated;
	}

	public void setDateupdated(Date dateupdated) {
		this.dateupdated = dateupdated;
	}

	public com.a2nine.requestapi.model.domain.Approver toDomainObject() {
		return new com.a2nine.requestapi.model.domain.Approver(this.id, this.name,
				new Organisation(this.orgname, this.orgcode), this.email, this.phone, this.isactive, this.dateupdated);
	}

}
