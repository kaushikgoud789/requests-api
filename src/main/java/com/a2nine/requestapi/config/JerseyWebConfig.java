package com.a2nine.requestapi.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.a2nine.requestapi.infrastructure.rest.RequestController;

@Configuration
@ApplicationPath("/requests")
public class JerseyWebConfig extends ResourceConfig {

	public JerseyWebConfig() {
		register(RequestController.class);
	}

}
